##
## Makefile for allum1 in /home/wu_d/epitech/tek1/s2/c_elem/allum1
## 
## Made by mauhoi wu
## Login   <wu_d@epitech.net>
## 
## Started on  Mon Feb 11 15:34:22 2013 mauhoi wu
## Last update Sun Feb 17 16:29:00 2013 mauhoi wu
##

NAME	= allum1

RLIB	= lib

RSRC	= src

SRC	= \
	$(RSRC)/main.c \
	$(RSRC)/my.c \
	$(RSRC)/my_check.c \
	$(RSRC)/my_getnbr.c \
	$(RSRC)/map.c \
	$(RSRC)/s_fun.c \
	$(RSRC)/move.c

OBJ	= $(SRC:.c=.o)

CC	= cc

RM	= rm -rvf

all: $(NAME)

$(NAME): $(OBJ)
	$(CC) -o $(NAME) $(OBJ) -lncurses

warn:	$(OBJ)
	$(CC) -o $(NAME) -Wall -Wextra $(OBJ) -lncurses

clean:
	$(RM) $(OBJ)

fclean: clean
	$(RM) $(NAME)

re: fclean all
