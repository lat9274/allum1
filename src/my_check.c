/*
** my_check.c for my in /home/wu_d/epitech/tek1/s2/c_elem/allum1/src
** 
** Made by mauhoi wu
** Login   <wu_d@epitech.net>
** 
** Started on  Mon Feb 11 15:55:18 2013 mauhoi wu
** Last update Mon Feb 11 15:57:47 2013 mauhoi wu
*/

#include "include/my.h"

int	my_isnum(char *str)
{
  int	i;

  i = 0;
  if (str)
    {
      while (str[i] != '\0')
	{
	  if (str[i] >= '0' && str[i] <= '9')
	    i = i + 1;
	  else
	    return (0);
	}
      return (1);
    }
  else
    return (0);
}
