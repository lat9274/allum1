/*
** main.c for allum in /home/wu_d/epitech/tek1/s2/c_elem/allum1/src
** 
** Made by mauhoi wu
** Login   <wu_d@epitech.net>
** 
** Started on  Mon Feb 11 15:53:09 2013 mauhoi wu
** Last update Sun Feb 17 16:53:15 2013 mauhoi wu
*/

#include <ncurses.h>
#include "include/my.h"
#include "include/struct.h"

void		init_st(t_note *n)
{
  n->size = 0;
  n->x = 0;
  n->y = 0;
  n->re = 0;
}

void		put_map(t_note *n)
{
  erase();
  n->y = 0;
  while (n->y < n->size)
    {
      n->x = 0;
      while (n->map[n->y][n->x] != '\0')
	{
	  addch(n->map[n->y][n->x]);
	  refresh();
	  n->x++;
	}
      addch('\n');
      n->y++;
    }
}

void		put_win(t_note *n)
{
  int		key;

  initscr();
  keypad(stdscr,TRUE);
  addstr("press a key to continu\n's' to select\n'q' to exit\n'd' to finish");
  while (42)
    {
      key = getch();
      /*if (key == KEY_UP)
	move_up(n);*/
      if (key == KEY_DOWN)
	move_down(n);
      /*
      else if (key == KEY_LEFT)
	move_left(n);
      else if (key == KEY_RIGHT)
      move_right(n);*/
      else if (key == 'd')
	n->re = 0;
      else if (key == 's')
	remove_allum(n);
      else if (key == 'q')
	ncu_exit("exit\n", n);
      else
	put_map(n);
    }
}

int		main(int ac, char **av)
{
  t_note	*n;

  if (ac == 2 && my_isnum(av[1]))
    {
      n = my_malloc(sizeof(*n));
      init_st(n);
      if ((n->size = my_getnbr(av[1])) > 1)
	{
	  map_alloc(n, n->size);
	  c_map(n, n->size);
	  put_win(n);
	}
      else
	my_putstr("usage: $>./allum1 [$(allum_s_num) > 1]\n");
    }
  else
    my_putstr("usage: $>./allum1 [$(allum_s_num) > 1]\n");
  return (0);
}
