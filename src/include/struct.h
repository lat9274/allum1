/*
** struct.h for allum1 in /home/wu_d/s2/c_elem/allum1/src/include
** 
** Made by mauhoi wu
** Login   <wu_d@epitech.net>
** 
** Started on  Wed Feb 13 14:19:08 2013 mauhoi wu
** Last update Sun Feb 17 16:04:39 2013 mauhoi wu
*/

#ifndef STRUCT_H_
# define STRUCT_H_

typedef struct	s_note
{
  char		**map;
  int		y;
  int		size;
  int		x;
  int		re;
}		t_note;

#endif /* !STRCUCT_H_ */
