/*
** allum.h for allum1 in /home/wu_d/epitech/tek1/s2/c_elem/allum1/src/include
** 
** Made by mauhoi wu
** Login   <wu_d@epitech.net>
** 
** Started on  Mon Feb 11 15:51:45 2013 mauhoi wu
** Last update Sun Feb 17 16:37:39 2013 mauhoi wu
*/

#ifndef ALLUM_H_
# define ALLUM_H_

#include "struct.h"

void	ncu_putstr(char *str);
void	ncu_exit(char *str, t_note *n);
int	search(t_note *n);
void	move_down(t_note *n);
void	move_up(t_note *n);
void	move_left(t_note *n);
void	move_right(t_note *n);

#endif /* !ALLUM_H_ */
