/*
** my.h for my in /home/wu_d/epitech/tek1/s2/c_elem/allum1/src/include
** 
** Made by mauhoi wu
** Login   <wu_d@epitech.net>
** 
** Started on  Mon Feb 11 15:45:23 2013 mauhoi wu
** Last update Fri Feb 15 15:03:01 2013 mauhoi wu
*/

#ifndef MY_H_
# define MY_H_

void	my_putchar(char c);
int	my_strlen(char *str);
void	my_putstr(char *str);
void	*my_malloc(int size);
int	my_isnum(char *str);
int	my_getnbr(char *str);

#endif /* !MY_H_ */
