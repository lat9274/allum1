/*
** move.c for allum1 in /media/epitech/wu_d/tek1/s2/c_elem/allum1/src
** 
** Made by mauhoi wu
** Login   <wu_d@epitech.net>
** 
** Started on  Sun Feb 17 16:04:11 2013 mauhoi wu
** Last update Sun Feb 17 16:50:18 2013 mauhoi wu
*/

#include "include/struct.h"

int	search(t_note *n)
{
  n->y = 0;
  while (n->y < n->size)
    {
      n->x = 0;
      while (n->map[n->y][n->x] != '\0')
	{
	  if (n->map[n->y][n->x] == 'I')
	    return (0);
	  n->x++;
	}
      n->y++;
    }
  return (1);
}

void	move_down(t_note *n)
{
  if (!n->re && !(search(n)))
    {
      while (n->y >= 0 && n->y < n->size)
	n->y++;
      if (n->map[n->y][n->x] == '|')
	n->map[n->y][n->x] == 'I';
      n->re = 1;
    }
  put_map(n);
}

void	move_up(t_note *n)
{
  if (!n->re)
    {
      if (!(search(n)))
        {
          while (n->y >= 0 && n->y < n->size)
            n->y--;
	  if (n->map[n->y][n->x] == '|')
            n->map[n->y][n->x] == 'I';
          n->re = 1;
        }
    }
  put_map(n);
}

void	move_left(t_note *n)
{
  if (!n->re)
    {
      if (!(search(n)))
	{
          while (n->map[n->y][n->x] != '|' && n->x >= 0)
            n->x--;
          if (n->map[n->y][n->x] == '|')
            n->map[n->y][n->x] == 'I';
          n->re = 1;
	}
    }
  put_map(n);
}

void	move_right(t_note *n)
{
  if (!n->re)
    {
      if (!(search(n)))
	{
	  while (n->map[n->y][n->x] != '|' && n->map[n->y][n->x] != '\0')
	    n->x++;
	  if (n->map[n->y][n->x] == '|')
	    n->map[n->y][n->x] == 'I';
	  n->re = 1;
	}
    }
  put_map(n);
}
