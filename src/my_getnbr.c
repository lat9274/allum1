/*
** my_getnbr.c for my_getnbr in /home/wu_d//local/colle_02
** 
** Made by mauhoi wu
** Login   <wu_d@epitech.net>
** 
** Started on  Mon Oct 22 09:52:02 2012 mauhoi wu
** Last update Mon Feb 11 16:04:27 2013 mauhoi wu
*/

#include "include/my.h"

int     my_getnbr(char *str)
{
  int   i;
  int   neg;
  int   nbr;

  i = 0;
  neg = 1;
  while (str[i] != '\0' && (str[i] == '+' || str[i] == '-'))
    {
      if (str[i] == '-')
	neg = neg * -1;
      i = i + 1;
    }
  str = str + i;
  i = 0;
  nbr = 0;
  while (str[i] != '\0' && (str[i] >= '0' && str[i] <= '9'))
    {
      nbr = nbr * 10;
      nbr = nbr + (str[i] - '0');
      i = i + 1;
    }
  return (nbr * neg);
}
