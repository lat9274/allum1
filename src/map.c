/*
** map.c for map in /home/wu_d/s2/c_elem/allum1/src
** 
** Made by mauhoi wu
** Login   <wu_d@epitech.net>
** 
** Started on  Wed Feb 13 16:45:05 2013 mauhoi wu
** Last update Sun Feb 17 16:39:27 2013 mauhoi wu
*/

#include <stdlib.h>
#include <ncurses.h>
#include "include/my.h"
#include "include/struct.h"

void		map_alloc(t_note *n, int size)
{
  int		y;

  y = 0;
  n->map = (char **)my_malloc(sizeof(char *) * size + 3);
  while (y <= size)
    {
      n->map[y] = (char *)my_malloc(size * 2 + 1);
      y = y + 1;
    }
}

void		c_map(t_note *n, int size)
{
  int		allum;

  allum = 1;
  while (n->y < n->size)
    {
      n->x = 0;
      while (n->x < size - 1)
	{
	  n->map[n->y][n->x] = ' ';
	  n->x++;
	}
      while (n->x < size - 1 + allum)
	{
	  if (n->y == 0)
	    n->map[n->y][n->x] = 'I';
	  else
	    n->map[n->y][n->x] = '|';
	  n->x++;
	}
      n->map[n->y][n->x] = '\0';
      allum = allum + 2;
      size--;
      n->y++;
    }
  n->map[n->y][n->x] = '\0';
}
