/*
** s_fun.c for allum1 in /media/epitech/wu_d/tek1/s2/c_elem/allum1/src
** 
** Made by mauhoi wu
** Login   <wu_d@epitech.net>
** 
** Started on  Sat Feb 16 18:24:07 2013 mauhoi wu
** Last update Sun Feb 17 16:51:49 2013 mauhoi wu
*/

#include <stdlib.h>
#include <ncurses.h>
#include "include/my.h"
#include "include/allum.h"
#include "include/struct.h"

void	ncu_putstr(char *str)
{
  erase();
  addstr(str);
  refresh();
}

void	ncu_exit(char *str, t_note *n)
{
  endwin();
  my_putstr(str);
  free(n);
  exit(0);
}

void	remove_allum(t_note *n)
{
  if (!(search(n)))
    n->map[n->y][n->x] = ' ';
}
