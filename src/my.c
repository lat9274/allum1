/*
** my.c for my in /home/wu_d/epitech/tek1/s2/c_elem/allum1/src/mylib
** 
** Made by mauhoi wu
** Login   <wu_d@epitech.net>
** 
** Started on  Mon Feb 11 15:36:28 2013 mauhoi wu
** Last update Fri Feb 15 15:02:52 2013 mauhoi wu
*/

#include <stdlib.h>
#include "include/my.h"

void	my_putchar(char c)
{
  write(1, &c, 1);
}

int	my_strlen(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    i = i + 1;
  return (i);
}

void	my_putstr(char *str)
{
  write(1, str, my_strlen(str));
}

void	*my_malloc(int size)
{
  void	*ptr;

  ptr = malloc(size);
  if (!ptr)
    {
      my_putstr("malloc fail");
      exit(0);
    }
  return (ptr);
}
